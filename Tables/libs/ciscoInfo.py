import telnetlib

from os import path, environ
dirname = path.dirname(__file__)
filename = path.join(dirname, '$TABLES/libs')
libs=str(environ['TABLES'])+"/libs"
logs=str(environ['TABLES'])+"/logs"

password="admin"

def connect(HOST):
    try:
        tn = telnetlib.Telnet(HOST)
        tn.read_until(b"Password: ")
        tn.write(password.encode('ascii')+b"\n")
    except:
        return(-1)
    return(tn)

def getInventory(HOST):
    ret=[]
    cmd = "show inventory"
    tn=connect(HOST)
    if(tn == -1):
        return(ret)
    tn.read_until(b">", 5)
    tn.write(cmd.encode('ascii') + b"\n")
    tn.write(b"exit\n")

    output=tn.read_until(b"EOF",1).decode('ascii')
    lines=output.split("NAME")
    lines=lines[1:]
    print(lines)
    for line in lines:
        line=line.replace('\r',"")
        line=line.replace('\n',",")
        line=line.replace(" ","")
        elements=line.split(":")
        tmp=elements[1].split(",")
        ret.append("NAME: "+tmp[0])
        tmp=elements[5].split(",")
        ret.append("SN: "+tmp[0])
    return(ret)

def enable(tn):
    tn.read_until(b">")
    tn.write(b"enable\n")
    tn.read_until(b"Password:")
    tn.write(b"admin\n")

def getConfig(HOST,name=0):
    if(name==0):
        name=HOST
    cmd = "copy running-config ftp:"
    ftp = "1.1.1.1"
    tn=connect(HOST)
    if(tn == -1):
        return(-1)
    print("Connection... Done")

    enable(tn)
    print("Enable... Done")

    tn.write(cmd.encode('ascii') + b"\n")
    tn.read_until(b"?",3)
    tn.write(ftp.encode('ascii') + b"\n")
    tn.read_until(b"?",3)
    tn.write(name.encode('ascii') + b"\n")
    tn.read_until(b"#",3)
    print("Success")

    tn.write(b"exit\n")
    return(0)
