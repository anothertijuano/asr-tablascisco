from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

from os import path, environ
dirname = path.dirname(__file__)
filename = path.join(dirname, '$TABLES/libs')
libs=str(environ['TABLES'])+"/libs"
logs=str(environ['TABLES'])+"/logs"


authorizer = DummyAuthorizer()
authorizer.add_user("cisco", "admin", str(environ['TABLES'])+"/ftp",perm='elradfmwMT')
authorizer.add_anonymous(str(environ['TABLES'])+"/ftp")

handler = FTPHandler
handler.authorizer = authorizer

server = FTPServer(("", 21), handler)
server.serve_forever()
