from os import system, path, environ
dirname = path.dirname(__file__)
filename = path.join(dirname, '$TABLES/libs')
logs=str(environ['TABLES'])+"/logs"

notifile=logs+"/notifications.log"

def webNotify(notification):
    fp=open(notifile,"a+")
    fp.write(str(notification)+"\n")
    fp.close()

