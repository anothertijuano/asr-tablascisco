from os import path, environ
dirname = path.dirname(__file__)
filename = path.join(dirname, '$TABLES/libs')
libs=str(environ['TABLES'])+"/libs"
logs=str(environ['TABLES'])+"/logs"

from sys import path
path.append(libs)

from ciscoInfo import getInventory
from network import getResponsiveHosts

output=logs+"/inventory.log"
out=open(output,"w+")

hosts=getResponsiveHosts()
for host in hosts:
    print(str(host)+":")
    inv=getInventory(host)
    for i in inv:
        print(i)
        out.write(str(i)+"\n")
out.close()
