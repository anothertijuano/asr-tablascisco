from os import path, environ
dirname = path.dirname(__file__)
filename = path.join(dirname, '$TABLES/libs')
libs=str(environ['TABLES'])+"/libs"
logs=str(environ['TABLES'])+"/logs"

import sys
sys.path.append(libs)

from ciscoInfo import *
from network import getResponsiveHosts

hosts=getResponsiveHosts()
for host in hosts:
    print(host)
    getConfig(host)
