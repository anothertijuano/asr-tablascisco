from os import path, environ
dirname = path.dirname(__file__)
filename = path.join(dirname, '$TABLES/libs')
libs=str(environ['TABLES'])+"/libs"
logs=str(environ['TABLES'])+"/logs"

from sys import path
path.append(libs)

from send import notify
from network import getHosts, ping

def pingList(ipList):
    #return list of inactive ips
    inactive=[]
    for ip in ipList:
        print("testing for "+str(ip))
        code=ping(ip)
        if (code!=0):
            print(str(ip)+" not responding")
            inactive.append(ip)
    return inactive

def list2file(List,path):
    fp=open(path, "w+")
    for i in List:
        fp.write(str(i))
        fp.write("\n")
    fp.close()

hosts=getHosts()
firstLevel=pingList(hosts)
secondLevel=pingList(firstLevel)
thirdLevel=pingList(secondLevel)
for host in thirdLevel:
    notify("Host: "+str(host)+" is down")

list2file(thirdLevel,logs+"/inactiveHosts.out")
