from flask import Flask, render_template, url_for, request, Response
from time import sleep
from os import path, listdir, environ, stat
dirname = path.dirname(__file__)
filename = path.join(dirname, '$TABLES/libs')
libs=str(environ['TABLES'])+"/libs"
logs=str(environ['TABLES'])+"/logs"
home=str(environ['TABLES'])

from sys import path
path.append(libs)

from network import getResponsiveHosts, getUnresponsiveHosts, file2list
app = Flask(__name__)

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/networkstatus')
def NetworkStatus():
    responsive=getResponsiveHosts()
    unresponsive=getUnresponsiveHosts()
    return render_template('networkstatus.html', responsive=responsive, unresponsive=unresponsive)

@app.route('/syslog')
def SyslogViewer():
    syslogs=file2list(logs+"/syslog.log")
    return render_template('syslog.html', syslogs=syslogs)

@app.route('/inventory')
def Inventory():
    inventory=file2list(logs+"/inventory.log")
    print(inventory)
    return render_template('inventory.html', inventory=inventory)

@app.route('/configurations')
def ConfigurationViewer():
    paths=listdir(home+"/ftp")
    files=[]
    for path in paths:
        files.append([path,file2list(home+"/ftp/"+path)])
    return render_template('configurations.html', files=files)

@app.route('/stream')
def stream():
    def eventStream():
        notifile=logs+"/notifications.log"
        time=0
        while True:
            sleep(1)
            newtime=stat(notifile).st_mtime
            if(newtime!=time):
                with open(notifile, "r") as f:
                    for line in f: pass
                time=newtime
                print(line) #this is the last line of the file
                yield "data: "+line+"\n\n"
    return Response(eventStream(), mimetype="text/event-stream")

@app.route('/notificationCenter')
def NotificationCenter():
    return render_template('notificationCenter.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
